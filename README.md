###Configuracion para la conexion de la base de datos

Para configuracion la conexion de la base de datos en el proyecto hay una clase llamada DBConnection
en el paquete models, ahi pondras el host de tu servidor que seria localhost, luego tu usuario, contrasena
y por supuesto el nombre de la base de datos que seria siempre papeleria, aqui te muestro cuales son las variables qe vas a modificar en la clase DBConnection.

```java
package models;

import java.sql.*;

public class DBConnection {

    private final String userDatabase = "tusuario";
    private final String password = "tucontrasena";
    private final String dataBase = "papeleria";
    private final String host = "localhost";

```

en mi caso si ves que el archivo tiene usuario y contrasena personalizado, la razon es que tengo un servidor dedicado para base de datos y esta estrictamente regulado por mi y nadie tiene acceso a el.