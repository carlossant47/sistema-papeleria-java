package clases;

import java.util.ArrayList;

public class Venta {

    private int idVenta;
    private String fecha;
    private int empleado;
    private Empleado emp;

    public Empleado getEmp() {
        return emp;
    }

    public void setEmp(Empleado emp) {
        this.emp = emp;
    }

    private int status;
    private float total;
    private ArrayList<Venta_Relacion> listaVenta;

    public Venta() {
        listaVenta = new ArrayList<>();
    }

    public ArrayList<Venta_Relacion> getListaVenta() {
        return listaVenta;
    }

    public void setListaVenta(ArrayList<Venta_Relacion> listaVenta) {
        this.listaVenta = listaVenta;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getEmpleado() {
        return empleado;
    }

    public void setEmpleado(int empleado) {
        this.empleado = empleado;
    }

    public float getTotal()
    {
        float total = 0.0f;
        for (Venta_Relacion ven : listaVenta)
        {
            total += ven.getSubtotal();
        }
        return total;
    }
}
