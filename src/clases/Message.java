package clases;

import javax.swing.*;

public class Message {

    public static void error(String title, String message)
    {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public static void information(String title, String message)
    {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
    }

    public static boolean desisionYesNo(String title, String message)
    {
        int result = JOptionPane.showConfirmDialog (null, message,title, JOptionPane.YES_NO_OPTION);
        if(result == JOptionPane.YES_OPTION)
            return true;
        else
            return false;
    }
}
