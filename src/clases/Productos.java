package clases;

import javax.swing.*;

public class Productos {
    private int id;
    private String nombreProducto;
    private float precio;
    private int tipoProducto;
    private int status;
    private Proveedores provedor;


    public Productos()
    {
        this.provedor = new Proveedores();
    }

    public Proveedores getProvedor() {
        return provedor;
    }

    public void setProvedor(Proveedores provedor) {
        this.provedor = provedor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public void setNombreProducto(JTextField nombreProducto)
    {
        this.nombreProducto = nombreProducto.getText();
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(int tipoProducto) {
        this.tipoProducto = tipoProducto;
    }
    public void setTipoProducto(TipoProducto tipoProducto)
    {
        this.tipoProducto = tipoProducto.getId();
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String toString()
    {
        return this.nombreProducto;
    }


}
