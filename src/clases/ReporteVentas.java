package clases;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import models.ModVenta;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ReporteVentas {
    private ArrayList<Venta> ventas;
    private ConfigSystem configSystem;
    private String tableVentas;
    private String baseReporte;
    private String htmlPDF;
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public ReporteVentas(ConfigSystem config, ArrayList<Venta> venta)
    {
        this.htmlPDF = "";
        this.configSystem = config;
        this.ventas = venta;
        tableVentas = Config.readFileString(getClass().getResourceAsStream("/assets/tableVentas.html"));
        this.baseReporte = Config.readFileString(getClass().getResourceAsStream("/assets/reporteVentas.html"));
        createVentaTable();

    }

    public void creatFile() throws IOException {
        File pdfDest = new File(this.filePath);
        ConverterProperties converterProperties = new ConverterProperties();
        HtmlConverter.convertToPdf(this.htmlPDF, new FileOutputStream(pdfDest), converterProperties);
    }

    private void createVentaTable()
    {
        StringBuilder tablas = new StringBuilder();
        baseReporte = baseReporte.replace("{{negocio}}", configSystem.getNombreNegociol());
        baseReporte = baseReporte.replace("{{direccion}}", configSystem.getDireccion());
        htmlPDF += baseReporte;
        this.baseReporte = null;
        for (Venta venta: ventas) {
            String tableVenta = tableVentas;
            if(venta.getListaVenta().size() > 0) {
                tableVenta = tableVenta.replace("{{venta}}", String.format("%010d", venta.getIdVenta()));
                tableVenta = tableVenta.replace("{{empleado}}", venta.getEmp().getNombre());
                tableVenta = tableVenta.replace("{{total}}", SystemClass.dineroFormato(venta.getTotal()));
                tableVenta = tableVenta.replace("{{fecha}}", venta.getFecha());
                tableVenta = tableVenta.replace("{{ventas}}", creteTableVentas(venta.getListaVenta()));
                tablas.append(tableVenta);
            }

        }
        this.htmlPDF = htmlPDF.replace("{{tablas}}", tablas.toString());
    }

    private String creteTableVentas(ArrayList<Venta_Relacion> venta_relacions)
    {
        String tableData = "";
        if(venta_relacions.size() > 0) {


            for (Venta_Relacion ven : venta_relacions) {
                tableData +=
                        "<tr>" +
                                "<td>" + String.format("%010d", ven.getVentar()) + "</td>" +
                                "<td>" + ven.getProducto().getNombreProducto() + "</td>" +
                                "<td>" + SystemClass.dineroFormato(ven.getProducto().getPrecio()) + "</td>" +
                                "<td>" + ven.getCantidad() + "</td>" +
                                "<td>" + ven.getSubtotal() + "</td>" +
                                "</tr>";
            }
        }
        else
        {
            tableData = "<h5>Esta venta esta vacia</h6>";
        }
        return tableData;
    }

    public String getHtmlPDF()
    {
        return this.htmlPDF;
    }

    public static void main(String[] agrs)
    {
        ModVenta mod = new ModVenta();
        ArrayList<Venta> ventas = mod.consultarVentas();
        ReporteVentas report = new ReporteVentas(new ConfigSystem(), ventas);
        System.out.println(report.getHtmlPDF());

    }



}
