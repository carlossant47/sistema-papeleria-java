package clases;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

public class ConfigSystem {

    private final String CONFIGFILENAMEPATH = Config.pathProgramData + "/config.json";
    private String impresora;
    private JSONObject jsonConfig;
    private String nombreNegociol;
    private String direccion;
    private String gracias;

    public ConfigSystem()
    {
        File file = new File(CONFIGFILENAMEPATH);
        jsonConfig = new JSONObject();
        if(file.exists())
        {
            String stringJson = Config.readFileDataSystem(CONFIGFILENAMEPATH);
            System.out.println(stringJson);
            jsonConfig = new JSONObject(stringJson);

            stringJson = null;
            JSONObject reporte = jsonConfig.getJSONObject("reporteConfig");
            this.direccion = reporte.getString("direccion");
            this.nombreNegociol = reporte.getString("negocio");
            this.gracias = reporte.getString("graciasmsg");
            reporte = null;
            this.impresora = jsonConfig.getString("impresora");

        }
        else
        {
            System.out.println("FILE CONFIG NOT FOUND");
            createFileConfig();
        }
    }

    public void setImpresora(String impresora) {
        this.impresora = impresora;

    }

    public String getNombreNegociol() {
        return nombreNegociol;
    }

    public void setNombreNegociol(String nombreNegociol) {
        this.nombreNegociol = nombreNegociol;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getGracias() {
        return gracias;
    }

    public void setGracias(String gracias) {
        this.gracias = gracias;
    }

    public String getImpresora()
    {
        return this.impresora;
    }

    private void createFileConfig()
    {
        JSONObject  reporteConfig = new JSONObject ();
        reporteConfig.put("direccion", "Av. Manuel");
        reporteConfig.put("graciasmsg", "Gracias por su compra");
        reporteConfig.put("negocio", "Papeleria Paty");
        jsonConfig.put("reporteConfig", reporteConfig);
        jsonConfig.put("impresora", "");
        saveFile();
    }

    public void reloadFile()
    {
        JSONObject  reporteConfig = new JSONObject ();
        reporteConfig.put("direccion", getDireccion());
        reporteConfig.put("graciasmsg", getGracias());
        reporteConfig.put("negocio", getNombreNegociol());
        jsonConfig.put("reporteConfig", reporteConfig);
        jsonConfig.put("impresora", getImpresora());
        saveFile();
    }

    public boolean saveFile()
    {
        boolean exito = false;
        try (FileWriter file = new FileWriter(CONFIGFILENAMEPATH)) {
            file.write(jsonConfig.toString());
            exito = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }




}
