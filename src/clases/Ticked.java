package clases;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class Ticked {
    private Venta venta;
    private String html;
    private ConfigSystem config;

    public Ticked(ConfigSystem config, Venta venta)
    {
        this.config = config;
        html = Config.readFileString(getClass().getResourceAsStream("/assets/reporte.html"));
        this.venta = venta;
        setData();
    }

    private void setData()
    {
        this.html = this.html.replace("{{negocio}}", config.getNombreNegociol());
        this.html = this.html.replace("{{direccion}}", config.getDireccion());
        this.html = this.html.replace("{{fecha}}", venta.getFecha());
        this.html = this.html.replace("{{tabladatos}}", createTable());
    }

    private String createTable()
    {
        StringBuilder htmlTable = new StringBuilder();
        for (Venta_Relacion ven: venta.getListaVenta())
        {
            htmlTable.append(
                    "<tr>" +
                    "<td class='cantidad'>"+ven.getCantidad()+"</td>" +
                    "<td class='producto'>"+ven.getProducto().getNombreProducto().toUpperCase()+"</td>" +
                    "<td class='precio'>"+SystemClass.dineroFormato(ven.getProducto().getPrecio())+"</td>" +
                    "</tr>");
        }
        return htmlTable.toString();
    }


    public void creatFile()
    {


        Date currentDate = new Date();
        long time =  (currentDate.getTime() / 1000);
        File pdfDest = new File(Config.PATH_TICKEDS + "/" + "ticked_" + time + "_reporte.pdf");
        ConverterProperties converterProperties = new ConverterProperties();
        try {
            HtmlConverter.convertToPdf(this.html, new FileOutputStream(pdfDest), converterProperties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}
