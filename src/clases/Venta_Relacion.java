package clases;

public class Venta_Relacion {

    private int ventar;
    private int idVenta;
    private Productos producto;
    private int cantidad;

    public Venta_Relacion()
    {
        this.producto = new Productos();
    }
    public Venta_Relacion(Productos producto) {
        this.producto = producto;
    }

    public int getVentar() {
        return ventar;
    }

    public void setVentar(int ventar) {
        this.ventar = ventar;
    }

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    public Productos getProducto() {
        return producto;
    }

    public void setProducto(Productos producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getSubtotal()
    {
        return this.producto.getPrecio() * this.cantidad;
    }
}
