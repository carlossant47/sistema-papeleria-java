package clases;

public class Estados {
    private int estado;
    private String desEstado;

    public Estados(int estado, String desEstado) {
        this.estado = estado;
        this.desEstado = desEstado;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDesEstado() {
        return desEstado;
    }

    public void setDesEstado(String desEstado) {
        this.desEstado = desEstado;
    }

    public String toString()
    {
        return this.desEstado;
    }
}
