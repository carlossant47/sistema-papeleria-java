package clases;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import java.io.*;

public class Config {
    private static final String nameProgram = "sistema_papeleria";
    public static final String pathProgramData = folderDataPath();
    public static final String PATH_TICKEDS = folderDataPath() + "/tickeds";

    public static String readFileDataSystem(String path)
    {
        String s = "";
        try {
            s = readFileString(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return s;
    }
    public static String readFileString(InputStream inputStream) {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return resultStringBuilder.toString();
    }


    public static void createDataFolder()
    {
        String FileFolder = System.getenv("APPDATA") + "\\" + nameProgram;
        System.out.println(folderDataPath());
        System.out.println("Searching for system");

        String os = System.getProperty("os.name").toUpperCase();
        if (os.contains("WIN")) {
            FileFolder = System.getenv("APPDATA") + "\\" + nameProgram;
            System.out.println("Found windows");
        }
        if (os.contains("MAC")) {
            FileFolder = System.getProperty("user.home") + "/Library/Application " + "Support"
                    + "Launcher";
            System.out.println("Found mac");
        }
        if (os.contains("NUX")) {
            FileFolder = System.getProperty("user.dir") + "." + nameProgram;
            System.out.println("Found linux");
        }

        System.out.println("Searching for resource folder");
        File directory = new File(FileFolder);

        if (directory.exists()) {
            System.out.println("Found folder");
        }

        if (!directory.exists()) {
            directory.mkdir();
            File folderReporte = new File(PATH_TICKEDS);
            folderReporte.mkdir();
            new ConfigSystem();

        }
    }
    public static PrintService findPrintService(String printerName) {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService printService : printServices) {
            if (printService.getName().trim().equals(printerName)) {
                return printService;
            }
        }
        return null;
    }



    private static void createFileConfig()
    {
        JSONObject json = new JSONObject();

        JSONObject  reporteConfig = new JSONObject ();
        reporteConfig.put("direccion", "Av. Manuel");
        reporteConfig.put("graciasmsg", "Gracias por su compra");
        json.put("reporteConfig", reporteConfig);
        json.put("impresora", "");
        System.out.println(json.toString());
        try (FileWriter file = new FileWriter(folderDataPath() + "/config.json")) {
            file.write(json.toString());
            System.out.println("Successfully Copied JSON Object to File...");
            System.out.println("\nJSON Object: " + json);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String folderDataPath()
    {

        String FileFolder = System.getenv("APPDATA") + "\\" + "Launcher";
        String os = System.getProperty("os.name").toUpperCase();
        if (os.contains("WIN")) {
            FileFolder = System.getenv("APPDATA") + "\\" + nameProgram;

        }
        if (os.contains("MAC")) {
            FileFolder = System.getProperty("user.home") + "/Library/Application " + "Support"
                    + nameProgram;

        }
        if (os.contains("NUX")) {
            FileFolder = System.getProperty("user.dir") + "." + nameProgram;
        }
        return FileFolder;


    }


}
