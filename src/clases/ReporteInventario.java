package clases;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import models.ModProductos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class ReporteInventario {
    private ConfigSystem config;
    private String html;
    private ArrayList<Productos> productos;
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public ReporteInventario(ConfigSystem config, ArrayList<Productos> productos)
    {
        this.config = config;
        this.productos = productos;
        this.html = "";
        this.html = Config.readFileString(getClass().getResourceAsStream("/assets/reporteInventario.html"));
        createData();
    }


    private void createData()
    {
        html = html.replace("{{negocio}}", config.getNombreNegociol());
        html = html.replace("{{direccion}}", config.getDireccion());
        html = html.replace("{{productos}}", createTable());
    }

    private String createTable()
    {
        StringBuilder builder = new StringBuilder();
        for (Productos prod: productos) {
            String row =
                    "<tr>"
                            + "<td>" + String.format("%010d", prod.getId()) + "</td>"
                            + "<td>" + prod.getNombreProducto() + "</td>"
                            + "<td>" + SystemClass.dineroFormato(prod.getPrecio()) + "</td>"
                            + "<td>" + prod.getProvedor().getEmpresa() + "</td>"
                            + "<td>" + ((prod.getStatus() == 1) ? "Activo" : "Baja") + "</td>"+
                            "</tr>";
            builder.append(row);
            row = null;
        }
        return builder.toString();
    }
    public void creatFile() throws IOException {
        File pdfDest = new File(this.filePath);
        ConverterProperties converterProperties = new ConverterProperties();
        HtmlConverter.convertToPdf(this.html, new FileOutputStream(pdfDest), converterProperties);
    }

    public String getHTML()
    { return html;}

    public static void main(String[] agrs)
    {
        ConfigSystem config = new ConfigSystem();
        ModProductos mod = new ModProductos();
        System.out.println(new ReporteInventario(config, mod.consultProductos()).getHTML());

    }


}
