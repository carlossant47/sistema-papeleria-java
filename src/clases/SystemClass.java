package clases;

import javax.swing.JComboBox;
import java.text.NumberFormat;
import java.util.Locale;

public class SystemClass {
    public static JComboBox<Estados> comboStatus()
    {
        JComboBox<Estados> model = new JComboBox<>();
        model.addItem(new Estados(1, "Activo"));
        model.addItem(new Estados(0, "Inactivo"));
        model.setSelectedIndex(0);
        return model;
    }

    public static String dineroFormato(int valor)
    {
        return NumberFormat.getCurrencyInstance(new Locale("es", "MX")).format(valor);
    }
    public static String dineroFormato(float valor)
    {
        return NumberFormat.getCurrencyInstance(new Locale("es", "MX")).format(valor);
    }
}
