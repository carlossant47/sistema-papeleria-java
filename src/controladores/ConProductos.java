package controladores;

import clases.*;
import models.ModProductos;
import vistas.ViewProducto;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ConProductos implements ActionListener, MouseListener {
    private ViewProducto view;
    private ModProductos model;


    public ConProductos(ViewProducto view, ModProductos model)
    {
        this.model = model;
        this.view = view;
        this.view.setTitle("Productos");
        //this.view.btnEliminar.setEnabled(false);
        this.view.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        generateGui();
        generateProductos();
        this.view.show();
    }

    public void generateProductos()
    {

        view.tbDatos.setModel(model.consultProductos(""));
        view.cmbProveedor.setModel(model.consultProveedores());
        view.cmbTipo.setModel(model.consultTipoProducto());

        //jTable1.setDefaultRenderer(Object.class, new CeldaRenderer(1));
    }

    public void generateGui()
    {
        view.tbDatos.addMouseListener(this);
        this.view.btnCancelar.addActionListener(this.btnCancelarAction());
        view.btnEditar.setEnabled(false);
        view.btnEliminar.setEnabled(false);
        view.btnCancelar.setEnabled(false);
        view.btnAgregar.addActionListener(this.btnAgregarAction());
        view.btnBuscar.addActionListener(this.btnBuscarAction());
        view.btnEditar.addActionListener(this.btnEditarAction());
        view.btnEliminar.addActionListener(this.btnEliminar());
    }

    public ActionListener btnEliminar()
    {
        return actionEvent -> {
            int id = Integer.parseInt(view.btnEditar.getToolTipText());
            int status = Integer.parseInt(view.btnEliminar.getToolTipText());
            if(model.cambiarStatus(id, status)){
                Message.information("Cambiar Estado", "Se cambio el status con exito");
                cancelar();
            }
        };
    }



    public ActionListener btnAgregarAction()
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Productos prod = new Productos();
                prod.setNombreProducto(view.txtNombre);
                prod.setPrecio(Float.parseFloat(view.txtPrecio.getText()));
                prod.setTipoProducto((TipoProducto) view.cmbTipo.getSelectedItem());
                prod.setProvedor((Proveedores) view.cmbProveedor.getSelectedItem());
                if(model.insertProducto(prod))
                {
                    Message.information("Modificar", "Se edito correctamente");
                }
                else
                {
                    Message.information("Error", "Se produjo un error al agragar");
                }
                generateProductos();

            }
        };
    }

    private ActionListener btnEditarAction() {
        return actionEvent -> {
            Productos prod = new Productos();
            prod.setNombreProducto(view.txtNombre);
            prod.setPrecio(Float.parseFloat(view.txtPrecio.getText()));
            prod.setTipoProducto((TipoProducto) view.cmbTipo.getSelectedItem());
            prod.setProvedor((Proveedores) view.cmbProveedor.getSelectedItem());
            prod.setId(Integer.parseInt(view.btnEditar.getToolTipText()));
            if(model.updateProducto(prod))
            {
                Message.information("Modificar", "Se agrego correctamente");
            }
            else
            {
                Message.information("Error", "Se produjo un error al agragar");

            }
            generateProductos();
            cancelar();
        };
    }

    private ActionListener btnBuscarAction()
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

            }
        };
    }

    private ActionListener btnCancelarAction()
    {
        return actionEvent -> {
            cancelar();
        };

    }

    public void cancelar()
    {
        view.btnAgregar.setEnabled(true);
        view.btnEditar.setEnabled(false);
        view.btnEditar.setToolTipText("");
        view.btnEliminar.setEnabled(false);
        view.btnCancelar.setEnabled(false);
        view.txtPrecio.setText("");
        view.txtNombre.setText("");
        view.cmbProveedor.setSelectedIndex(0);
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

    private void setData(Productos prod)
    {
        view.txtNombre.setText(prod.getNombreProducto());
        view.txtPrecio.setText(String.valueOf(prod.getPrecio()));

    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (view.tbDatos == mouseEvent.getSource()) {
            int index = view.tbDatos.getSelectedRow();
            System.out.println(index);
            if (index >= 0) {
                int id = (int) view.tbDatos.getValueAt(index, 0);//CUMLUMNA DEL ID
                if (Message.desisionYesNo("Error", "Deseas Editar este Producto")) {
                    Productos prod = model.buscarProductoID(id);
                    if (prod == null) {
                        Message.error("Error", "Se produjo un error al buscar el producto");
                    } else {
                        view.txtPrecio.setText(String.valueOf(prod.getPrecio()));
                        view.txtNombre.setText(prod.getNombreProducto());
                        view.cmbProveedor.setSelectedIndex(prod.getProvedor().getId() - 1);
                        if(prod.getStatus() == 1)
                        {
                            view.btnEliminar.setText("Desabilitar");
                        }
                        else
                        {
                            view.btnEliminar.setText("Habilitar");
                        }
                        view.btnAgregar.setEnabled(false);
                        view.btnEditar.setEnabled(true);
                        view.btnEditar.setToolTipText(String.valueOf(prod.getId()));
                        view.btnEliminar.setToolTipText(String.valueOf(prod.getStatus()));
                        view.btnEliminar.setEnabled(true);
                        view.btnCancelar.setEnabled(true);
                    }
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
