package controladores;

import clases.*;
import models.ModEmpleados;
import models.ModUsuarios;
import vistas.ViewUsuarios;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ConUsuarios implements MouseListener, ActionListener{
    private ViewUsuarios view;
    private ModUsuarios model;
    private ModEmpleados modelEmpleados;

    public ConUsuarios(ViewUsuarios view, ModUsuarios model)
    {
        this.view = view;
        this.modelEmpleados = new ModEmpleados();
        this.model = model;
        this.view.setTitle("Usuarios");
        this.view.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        generateUsuario();
        generateGui();
        this.view.pack();
        this.view.show();

    }

    public void generateUsuario()
    {

        view.tbDatos.setModel(model.consultar("", 1));
        view.cmbEmpleado.setModel(modelEmpleados.consultEmpleados());
    }

    public void generateGui()
    {
        view.tbDatos.addMouseListener(this);
        this.view.btnCancelar.addActionListener(this.btnCancelarAction());
        view.btnEditar.setEnabled(false);
        view.btnEliminar.setEnabled(false);
        view.btnCancelar.setEnabled(false);
        view.btnAgregar.addActionListener(this.btnAgregarAction());
        view.btnBuscar.addActionListener(this.btnBuscarAction());
        view.btnEditar.addActionListener(this.btnEditarAction());
        view.btnEliminar.addActionListener(this.btnEliminar());
    }

    public ActionListener btnEliminar()
    {
        return actionEvent -> {
            int id = Integer.parseInt(view.btnEditar.getToolTipText());
            int status = Integer.parseInt(view.btnEliminar.getToolTipText());
            if(model.cambiarStatus(id, status)){
                Message.information("Cambiar Estado", "Se cambio el status con exito");
                cancelar();
            }
        };
    }



    public ActionListener btnAgregarAction()
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Usuario usuario = new Usuario();
                usuario.setUser(view.txtUsuario.getText());
                usuario.setPass(view.txtPass.getText());
                usuario.setEmp((Empleado) view.cmbEmpleado.getSelectedItem());

                if(model.insertar(usuario))
                {
                    Message.information("Modificar", "Se edito correctamente");
                }
                else
                {
                    Message.information("Error", "Se produjo un error al agragar");
                }
                generateUsuario();

            }
        };
    }

    private ActionListener btnEditarAction() {
        return actionEvent -> {
            Usuario usuario = new Usuario();
            usuario.setUser(view.txtUsuario.getText());
            usuario.setPass(view.txtPass.getText());
            usuario.setEmp((Empleado) view.cmbEmpleado.getSelectedItem());
            usuario.setIdUsuario(Integer.parseInt(view.btnEditar.getToolTipText()));
            if(model.actualizar(usuario))
            {
                Message.information("Modificar", "Se agrego correctamente");
            }
            else
            {
                Message.information("Error", "Se produjo un error al agragar");

            }
            generateUsuario();
            cancelar();
        };
    }

    private ActionListener btnBuscarAction()
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

            }
        };
    }

    private ActionListener btnCancelarAction()
    {
        return actionEvent -> {
            cancelar();
        };

    }

    public void cancelar()
    {
        view.btnAgregar.setEnabled(true);
        view.btnEditar.setEnabled(false);
        view.btnEditar.setToolTipText("");
        view.btnEliminar.setEnabled(false);
        view.btnCancelar.setEnabled(false);
        view.txtPass.setText("");
        view.txtUsuario.setText("");
        view.cmbEmpleado.setSelectedIndex(0);
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

    private void setData(Productos prod)
    {
        view.txtUsuario.setText(prod.getNombreProducto());
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (view.tbDatos == mouseEvent.getSource()) {
            int index = view.tbDatos.getSelectedRow();
            System.out.println(index);
            if (index >= 0) {
                int id = (int) view.tbDatos.getValueAt(index, 0);//CUMLUMNA DEL ID
                if (Message.desisionYesNo("Error", "Deseas Editar este Producto")) {
                    Usuario prod = model.buscarId(id);
                    if (prod == null) {
                        Message.error("Error", "Se produjo un error al buscar el producto");
                    } else {
                        view.txtUsuario.setText(prod.getUser());
                        if(prod.getStatus() == 1)
                        {
                            view.btnEliminar.setText("Desabilitar");
                        }
                        else
                        {
                            view.btnEliminar.setText("Habilitar");
                        }
                        view.btnAgregar.setEnabled(false);
                        view.btnEditar.setEnabled(true);
                        view.btnEditar.setToolTipText(String.valueOf(prod.getIdUsuario()));
                        view.btnEliminar.setToolTipText(String.valueOf(prod.getStatus()));
                        view.btnEliminar.setEnabled(true);
                        view.btnCancelar.setEnabled(true);
                    }
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

}
