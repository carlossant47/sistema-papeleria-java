package controladores;

import clases.*;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import models.ModProductos;

import models.ModVenta;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import vistas.ViewVentas;
import vistas.modals.FrmAddProducto;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Sides;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class ConVentas implements Runnable {

    private ViewVentas view;
    private ModProductos model;
    private Venta venta;
    private ConfigSystem confing = new ConfigSystem();
    private ModVenta modelVenta;
    private JPanel mainFrame;
    private Empleado empleado;
    public ConVentas(ViewVentas view, ModVenta model,JPanel mainFrame, Empleado emp) {
        //this.model = model;
        //venta.setListaVenta(new ArrayList<Venta_Relacion>());
        this.modelVenta = model;
        this.view = view;
        this.model = new ModProductos();
        this.empleado = emp;
        this.venta = new Venta();
        generateGUI();
        new CambiaPanel(mainFrame, this.view);
        this.mainFrame = mainFrame;
    }

    private void generateGUI()
    {
        view.lbEmpleado.setText("Lo antiende: " + empleado.getNombre());
        this.view.btnAdd.addActionListener(this.btnAddProducto());
        generateTable();
        this.view.btnEliminar.addActionListener(this.btnDeleteProducto());
        view.tbData.getTableHeader().setReorderingAllowed(false);
        this.view.btnTerminar.addActionListener(this.btnFinalizarAction());
        this.view.btnCancelar.addActionListener(actionEvent -> {
            if(Message.desisionYesNo("Cancelar Venta", "Deseas cancelar la venta?"))
                terminarVenta();
        });
        this.view.btnClose.addActionListener(actionEvent -> {
            new CambiaPanel(this.mainFrame, null);
            this.view = null;
            this.modelVenta = null;
            this.model = null;


        });
    }

    private ActionListener btnFinalizarAction()
    {
        return actionEvent -> {
            if(venta.getListaVenta().size() <= 0)
            {
                Message.error("Error", "No tiene ningun producto en la lista");
            }
            else {
                new Thread(this, "fin").start();
            }
        };
    }



    private ActionListener btnDeleteProducto()
    {
        return actionEvent -> {
            if(view.tbData.getSelectedRow() <  0)
            {
                Message.error("Error", "No ha seleccionado ningun producto");
            }
            else
            {

                this.venta.getListaVenta().remove(view.tbData.getSelectedRow());
                generateTable();
                new Thread(this, "total").start();
            }
        };

    }
    public void printPDF() throws PrintException, IOException, PrinterException {
        PDDocument document = PDDocument.load(new File("C:\\reporte.pdf"));
        PrintService myPrintService = Config.findPrintService(confing.getImpresora());
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPageable(new PDFPageable(document));
        job.setPrintService(myPrintService);
        job.print();
    }

    public void generateTable()
    {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Venta");
        model.addColumn("Codigo de Producto");
        model.addColumn("Producto");
        model.addColumn("Precio");
        model.addColumn("Subtotal");
        if(venta.getListaVenta().size() > 0) {
            int count = 0;
            for (Venta_Relacion ven : venta.getListaVenta()) {
                model.addRow(new Object[]
                        {
                                count + 1,
                                ven.getProducto().getId(),
                                ven.getProducto().getNombreProducto(),
                                SystemClass.dineroFormato(ven.getProducto().getPrecio()),
                                SystemClass.dineroFormato(ven.getSubtotal())
                        });
                count++;
            }
        }
        view.tbData.setModel(model);

    }

    public ActionListener btnAddProducto()
    {
        return actionEvent -> {
            ConModalAddProd modal = new ConModalAddProd(new FrmAddProducto(), model);
            if(modal.getVenta() != null)
            {
                System.out.println(modal.getVenta().getCantidad());
                venta.getListaVenta().add(modal.getVenta());
                generateTable();
                new Thread(this, "total").start();
            }
            else
            {
                System.out.println("No se a selecionado ningun producto");
            }

            modal = null;




        };
    }



    @Override
    public void run() {
        if(Thread.currentThread().getName().equals("total"))
        {
            view.lbTotal.setText("Total "+SystemClass.dineroFormato(venta.getTotal()));
        }
        else if(Thread.currentThread().getName().equals("fin"))
        {
            String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            venta.setFecha(date);// La cadena de formato de fecha se pasa como un argumento al objeto
            if(modelVenta.insertVentas(venta))
            {
                Message.information("Venta", "La venta se realizo con exito");
                createPDFTicked();
                terminarVenta();

            }
            else
            {
                Message.information("Error", "No se pudo completar el registro");

            }
        }else if(Thread.currentThread().getName().equals("ticked"))
        {

        }
    }

    public void createPDFTicked()
    {
        Ticked ticked = new Ticked(confing, venta);
        ticked.creatFile();
    }
    private void terminarVenta()
    {
        this.venta = new Venta();
        generateTable();
        new Thread(this, "total").start();
    }
}
