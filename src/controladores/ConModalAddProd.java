package controladores;

import clases.Message;
import clases.Productos;
import clases.Venta_Relacion;
import models.ModProductos;
import vistas.modals.FrmAddProducto;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.text.NumberFormat;
import java.util.Locale;

public class ConModalAddProd implements Runnable{

    private FrmAddProducto view;
    private ModProductos model;
    private Venta_Relacion venta;
    public ConModalAddProd(FrmAddProducto view, ModProductos model)
    {
        this.venta = null;
        this.model = model;
        this.view = view;
        this.view.setLocationRelativeTo(null);
        this.view.setTitle("Ventas");
        //this.view.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.view.setModal(true);
        generateGUI();
        this.view.show();
    }

    private void generateGUI()
    {
            new Thread(this, "loadprod").start();

        this.view.cmbProducto.addItemListener(itemEvent -> {
            if(itemEvent.getStateChange() == ItemEvent.SELECTED)
            {

                Productos prod = (Productos) itemEvent.getItem();
                String precio = NumberFormat.getCurrencyInstance(new Locale("en", "US"))
                        .format(prod.getPrecio());
                view.lbPrecio.setText("Precio " + precio);
                if(!view.txtCantidad.getText().matches(""))
                {
                    calculateSubtotal(prod.getPrecio());
                }

            }
        });
        this.view.btnAgregar.addActionListener(this.btnAddAction());

    }


    private ActionListener btnAddAction()
    {
        return actionEvent -> {
            if(view.cmbProducto.getSelectedIndex() <= -1)
            {
                Message.error("Error", "Seleccione un Producto");
            }
            else if(view.txtCantidad.getText().matches(""))
            {
                Message.error("Error", "Ingrese una cantidad");
            }
            else
            {
                Venta_Relacion ven = new Venta_Relacion();
                ven.setProducto((Productos) view.cmbProducto.getSelectedItem());
                ven.setCantidad(this.getCantidad());

                this.venta = ven;
                this.view.dispose();
            }
        };
    }

    private void calculateSubtotal(float precio) {
        float subtotal = precio * Float.parseFloat(view.txtCantidad.getText());
        String sub = NumberFormat.getCurrencyInstance(new Locale("es", "MX")).format(subtotal);
        System.out.println(sub);
        view.lbPrecio.setText("Precio " + sub);
    }


    public Venta_Relacion getVenta() {
        return venta;
    }

    public void setVenta(Venta_Relacion venta) {
        this.venta = venta;
    }

    private int getCantidad()
    {
        return Integer.parseInt(this.view.txtCantidad.getText());
    }

    @Override
    public void run() {
        if(Thread.currentThread().getName().equals("loadprod"))
        {
            this.view.cmbProducto.setModel(this.model.consultarProductos());
        }
    }
}
