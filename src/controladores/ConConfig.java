package controladores;

import clases.ConfigSystem;
import vistas.ViewConfig;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.*;
import java.awt.event.ActionListener;

public class ConConfig {
    private ViewConfig view;

    private ConfigSystem config;

    public ConConfig(ViewConfig view) {
        config = new ConfigSystem();
        this.view = view;
        this.view.setTitle("Ventas");
        this.view.setModal(true);
        this.view.setLocationRelativeTo(null);
        loadPrinters();
        loadConfig();
        this.view.btnAceptar.addActionListener(btnAceptarAction());
        this.view.show();
    }

    private ActionListener btnAceptarAction() {
        return actionEvent -> {
            config.setDireccion(view.txtDireccion.getText());
            config.setImpresora((String) view.cmbImpresora.getSelectedItem());
            config.setGracias(view.txtGracias.getText());
            config.setNombreNegociol(view.txtNegocio.getText());
            config.reloadFile();
        };

    }

    private void loadConfig() {
        view.txtDireccion.setText(config.getDireccion());
        view.txtGracias.setText(config.getGracias());
        view.txtNegocio.setText(config.getNombreNegociol());
        System.out.println(config.getImpresora());
        compareComboBox(view.cmbImpresora, config.getImpresora());
    }

    private void loadPrinters() {
        int i;
        DocFlavor fl = null;
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel();
        PrintService printService[] = PrintServiceLookup.lookupPrintServices(fl, null);
        for (i = 0; i < printService.length; i++) {
            model.addElement(printService[i].getName());
        }
        view.cmbImpresora.setModel(model);

    }

    public void compareComboBox(JComboBox cmb, String key) {
        for (int x = 0; x < cmb.getItemCount(); x++) {
            if (cmb.getItemAt(x).toString().trim().equals(key)) {
                cmb.setSelectedIndex(x);
            }
            else
            {
                System.out.println("No es igual");
            }
        }
    }


}
