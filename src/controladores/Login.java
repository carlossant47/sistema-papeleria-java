package controladores;

import clases.Empleado;
import clases.Message;
import jdk.nashorn.internal.scripts.JO;
import vistas.FrmLogin;
import vistas.ViewMenu;

import javax.swing.*;
import java.awt.event.*;

public class Login {
    private FrmLogin view;
    public Login(FrmLogin view)
    {
        this.view = view;
        this.view.btnLogin.addActionListener(this.btnLoginAction());
        this.view.txtUsuario.addKeyListener(this.txtEvents());
        this.view.txtContraseña.addKeyListener(this.txtEvents());
        this.view.setTitle("");
        this.view.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.view.show();
    }

    private KeyAdapter txtEvents()
    {
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if(keyEvent.getKeyCode() == KeyEvent.VK_ENTER)
                {
                    login();
                }
                super.keyPressed(keyEvent);
            }
        };
    }


    private ActionListener btnLoginAction()
    {
        return actionEvent -> {
            login();
        };

    }

    private void login()
    {
        if(view.txtUsuario.getText().matches("") || view.txtUsuario.getText().matches(""))
            Message.error("Error", "No puedes dejar los campos vacios");
        else
        {
            String usuario = view.txtUsuario.getText(), password = view.txtContraseña.getText();
            models.Login login = new models.Login(usuario, password);
            Empleado emp = login.loginAction();
            if(emp == null)
            {
                Message.error("Error", "Contraseña o Usuario Incorrectos");
            }
            else if(emp.getUsuario().getStatus() == 0){
                Message.error("Error", "Lo sentimos pero este usuario a sido desabilitado");
            }
            else
            {
                Message.information("Login", "Bienvenido " + emp.getNombre());
                new ConMenu(new ViewMenu(), emp);
                this.view.dispose();
            }
        }
    }

}
