package controladores;

import clases.Empleado;
import clases.Message;
import clases.Productos;
import clases.Usuario;
import javafx.scene.input.KeyCode;
import models.ModEmpleados;
import vistas.ViewEmpleados;

import javax.swing.*;
import java.awt.event.*;


///
///Autor Carlos Santiaogo
//FROM MAZATLAN SINALOA MEXICO
public class ConEmpleados implements MouseListener, ActionListener {

    private ViewEmpleados view;
    private ModEmpleados model;


    public ConEmpleados(ViewEmpleados view, ModEmpleados model) {
        this.view = view;
        this.model = model;
        this.view.setTitle("Empleados");
        this.view.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.generate();
        this.generateGui();
        this.view.show();
    }

    public void generate()
    {

        view.tbDatos.setModel(model.consultar(""));
    }

    public void generateGui()
    {

        view.tbDatos.addMouseListener(this);
        this.view.btnCancelar.addActionListener(this.btnCancelarAction());
        view.btnEditar.setEnabled(false);
        view.btnEliminar.setEnabled(false);
        view.btnCancelar.setEnabled(false);
        view.btnAgregar.addActionListener(this.btnAgregarAction());
        view.btnBuscar.addActionListener(this.btnBuscarAction());
        view.btnEditar.addActionListener(this.btnEditarAction());
        view.btnEliminar.addActionListener(this.btnEliminar());
        view.btnBuscar.addActionListener(btnBuscar());
        view.txtBuscar.addKeyListener(txtBuscar());
    }

    public ActionListener btnEliminar()
    {
        return actionEvent -> {
            int id = Integer.parseInt(view.btnEditar.getToolTipText());
            int status = Integer.parseInt(view.btnEliminar.getToolTipText());
            if(model.cambiarStatus(id, status)){
                Message.information("Cambiar Estado", "Se cambio el status con exito");
                cancelar();
            }
        };
    }

    public ActionListener btnBuscar()
    {
        return actionEvent -> {
            String busqueda = view.txtBuscar.getText();
            view.tbDatos.setModel(model.consultar(busqueda));
        };
    }

    public KeyAdapter txtBuscar()
    {
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if(keyEvent.getKeyCode() == KeyEvent.VK_ENTER)
                {
                    String busqueda = view.txtBuscar.getText();
                    view.tbDatos.setModel(model.consultar(busqueda));
                }
                super.keyPressed(keyEvent);
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                if(keyEvent.getKeyCode() == KeyEvent.VK_BACK_SPACE)
                {
                    if(view.txtBuscar.getText().matches(""))
                    {
                        view.tbDatos.setModel(model.consultar(""));
                    }

                }
                super.keyReleased(keyEvent);
            }
        };
    }



    public ActionListener btnAgregarAction()
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Empleado obj = new Empleado();
                obj.setNombre(view.txtNombre.getText());
                obj.setApellidos(view.txtApellidos.getText());
                obj.setCorreo(view.txtCorreo.getText());
                obj.setTelefono(view.txtTelefono.getText());

                if(model.insertEmpleado(obj))
                {
                    Message.information("Modificar", "Se Guardo correctamente");
                }
                else
                {
                    Message.information("Error", "Se produjo un error al agragar");
                }
                generate();

            }
        };
    }

    private ActionListener btnEditarAction() {
        return actionEvent -> {
            Empleado obj = new Empleado();
            obj.setNombre(view.txtNombre.getText());
            obj.setApellidos(view.txtApellidos.getText());
            obj.setCorreo(view.txtCorreo.getText());
            obj.setTelefono(view.txtTelefono.getText());
            obj.setIdEmpleado(Integer.parseInt(view.btnEditar.getToolTipText()));
            if(model.updateEmpleado(obj))
            {
                Message.information("Modificar", "Se agrego correctamente");
            }
            else
            {
                Message.information("Error", "Se produjo un error al agragar");

            }
            generate();
            cancelar();
        };
    }

    private ActionListener btnBuscarAction()
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

            }
        };
    }

    private ActionListener btnCancelarAction()
    {
        return actionEvent -> {
            cancelar();
        };

    }

    public void cancelar()
    {
        view.btnAgregar.setEnabled(true);
        view.btnEditar.setEnabled(false);
        view.btnEditar.setToolTipText("");
        view.btnEliminar.setEnabled(false);
        view.btnCancelar.setEnabled(false);
        view.txtApellidos.setText("");
        view.txtNombre.setText("");
        view.txtTelefono.setText("");
        view.txtCorreo.setText("");
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

    private void setData(Empleado prod)
    {
        view.txtNombre.setText(prod.getNombre());
        view.txtApellidos.setText(prod.getApellidos());
        view.txtTelefono.setText(prod.getTelefono());
        view.txtCorreo.setText(prod.getCorreo());

    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (view.tbDatos == mouseEvent.getSource()) {
            int index = view.tbDatos.getSelectedRow();
            System.out.println(index);
            if (index >= 0) {
                int id = (int) view.tbDatos.getValueAt(index, 0);//CUMLUMNA DEL ID
                if (Message.desisionYesNo("Error", "Deseas Editar este Producto")) {
                    Empleado prod = model.buscarId(id);
                    if (prod == null) {
                        Message.error("Error", "Se produjo un error al buscar el producto");
                    } else {
                        setData(prod);
                        if(prod.getStatus() == 1)
                        {
                            view.btnEliminar.setText("Desabilitar");
                        }
                        else
                        {
                            view.btnEliminar.setText("Habilitar");
                        }
                        view.btnAgregar.setEnabled(false);
                        view.btnEditar.setEnabled(true);
                        view.btnEditar.setToolTipText(String.valueOf(prod.getIdEmpleado()));
                        view.btnEliminar.setToolTipText(String.valueOf(prod.getStatus()));
                        view.btnEliminar.setEnabled(true);
                        view.btnCancelar.setEnabled(true);
                    }
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
