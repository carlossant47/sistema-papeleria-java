package controladores;

import clases.CambiaPanel;
import clases.Empleado;
import models.*;
import vistas.*;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ConMenu {
    private ViewMenu view;
    private Empleado empleado;
    public ConMenu(ViewMenu view, Empleado empleado)
    {

        this.view = view;
        this.empleado = empleado;
        view.pContent.setLayout(new javax.swing.BoxLayout(view.pContent, javax.swing.BoxLayout.LINE_AXIS));
        this.view.setTitle("Ventas");
        this.view.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.view.setExtendedState(this.view.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        this.view.pack();
        generateGUI();
        this.view.show();
    }

    private void generateGUI() {
        view.btnConfiguracion.addActionListener(btnConfigAction());
        view.btnVentas.addActionListener(btnVentas());
        view.btnProductos.addActionListener(actionEvent -> new ConProductos(new ViewProducto(), new ModProductos()));
        view.btnReportes.addActionListener(this.btnReportes());
        view.btnUsuarios.addActionListener(actionEvent -> {
            new ConUsuarios(new ViewUsuarios(), new ModUsuarios());
        });
        view.btnProveedores.addActionListener(this.btnProveedores());
        view.btnEmpleados.addActionListener(this.btnEmpleados());
    }

    private ActionListener btnProveedores()
    {
        return actionEvent -> {
            new ConProveedores(new ViewProveedores(), new ModProveedores());
        };
    }

    private ActionListener btnConfigAction()
    {
        return actionEvent -> {
            new ConConfig(new ViewConfig());
        };
    }
    private ActionListener btnEmpleados()
    {
        return actionEvent -> {
            new ConEmpleados(new ViewEmpleados(), new ModEmpleados());
        };
    }

    private ActionListener btnVentas()
    {
        return actionEvent ->
        {
            new ConVentas(new ViewVentas(), new ModVenta(),view.pContent, empleado);
        };
    }

    private ActionListener btnReportes()
    {
        return actionEvent -> {
            new ConReportes(new ViewReportes());
        };
    }



}
