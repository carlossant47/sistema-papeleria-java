package controladores;

import clases.Empleado;
import clases.Message;
import clases.Proveedores;
import models.ModProveedores;
import vistas.ViewProveedores;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ConProveedores implements MouseListener, ActionListener {

    private ViewProveedores view;
    private ModProveedores model;

    public ConProveedores(ViewProveedores view, ModProveedores model) {
        this.view = view;
        this.model = model;
        this.view.setTitle("Empleados");
        this.view.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.generate();
        this.generateGui();
        this.view.show();
    }

    public void generate()
    {

        view.tbDatos.setModel(model.consultar(""));
    }

    public void generateGui()
    {
        view.tbDatos.addMouseListener(this);
        this.view.btnCancelar.addActionListener(this.btnCancelarAction());
        view.btnEditar.setEnabled(false);
        view.btnEliminar.setEnabled(false);
        view.btnCancelar.setEnabled(false);
        view.btnAgregar.addActionListener(this.btnAgregarAction());
        view.btnBuscar.addActionListener(this.btnBuscarAction());
        view.btnEditar.addActionListener(this.btnEditarAction());
        view.btnEliminar.addActionListener(this.btnEliminar());
    }

    public ActionListener btnEliminar()
    {
        return actionEvent -> {
            int id = Integer.parseInt(view.btnEditar.getToolTipText());
            int status = Integer.parseInt(view.btnEliminar.getToolTipText());
            if(model.cambiarStatus(id, status)){
                Message.information("Cambiar Estado", "Se cambio el status con exito");
                cancelar();
                generate();
            }
        };
    }



    public ActionListener btnAgregarAction()
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Proveedores obj = new Proveedores();
                obj.setEmpresa(view.txtNombre.getText());
                obj.setDireccion(view.txtDirecion.getText());
                obj.setTelefono(view.txtTelefono.getText());

                if(model.insertar(obj))
                {
                    Message.information("Guardar", "Se Guardo correctamente");
                }
                else
                {
                    Message.information("Error", "Se produjo un error al agragar");
                }
                generate();

            }
        };
    }

    private ActionListener btnEditarAction() {
        return actionEvent -> {
            Proveedores obj = new Proveedores();
            obj.setEmpresa(view.txtNombre.getText());
            obj.setDireccion(view.txtDirecion.getText());
            obj.setTelefono(view.txtTelefono.getText());

            obj.setId(Integer.parseInt(view.btnEditar.getToolTipText()));
            if(model.actualizar(obj))
            {
                Message.information("Modificar", "Se agrego correctamente");
            }
            else
            {
                Message.information("Error", "Se produjo un error al agragar");

            }
            generate();
            cancelar();
        };
    }

    private ActionListener btnBuscarAction()
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

            }
        };
    }

    private ActionListener btnCancelarAction()
    {
        return actionEvent -> {
            cancelar();
        };

    }

    public void cancelar()
    {
        view.btnAgregar.setEnabled(true);
        view.btnEditar.setEnabled(false);
        view.btnEditar.setToolTipText("");
        view.btnEliminar.setEnabled(false);
        view.btnCancelar.setEnabled(false);
        view.txtNombre.setText("");
        view.txtTelefono.setText("");
        view.txtDirecion.setText("");
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

    private void setData(Proveedores prod)
    {
        view.txtNombre.setText(prod.getEmpresa());
        view.txtTelefono.setText(prod.getTelefono());
        view.txtDirecion.setText(prod.getDireccion());

    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (view.tbDatos == mouseEvent.getSource()) {
            int index = view.tbDatos.getSelectedRow();
            System.out.println(index);
            if (index >= 0) {
                int id = (int) view.tbDatos.getValueAt(index, 0);//CUMLUMNA DEL ID
                if (Message.desisionYesNo("Error", "Deseas Editar este Producto")) {
                    Proveedores prod = model.buscarId(id);
                    if (prod == null) {
                        Message.error("Error", "Se produjo un error al buscar el producto");
                    } else {
                        setData(prod);
                        if(prod.getStatus() == 1)
                        {
                            view.btnEliminar.setText("Desabilitar");
                        }
                        else
                        {
                            view.btnEliminar.setText("Habilitar");
                        }
                        view.btnAgregar.setEnabled(false);
                        view.btnEditar.setEnabled(true);
                        view.btnEditar.setToolTipText(String.valueOf(prod.getId()));
                        view.btnEliminar.setToolTipText(String.valueOf(prod.getStatus()));
                        view.btnEliminar.setEnabled(true);
                        view.btnCancelar.setEnabled(true);
                    }
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
