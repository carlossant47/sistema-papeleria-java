package controladores;

import clases.*;
import models.ModProductos;
import models.ModVenta;
import vistas.ViewReportes;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class ConReportes {
    private ViewReportes view;

    public ConReportes(ViewReportes view) {
        this.view = view;
        this.view.setTitle("Reportes");
        generateGUI();
        this.view.pack();
        this.view.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.view.show();
    }

    public void generateGUI() {
        view.btnProducto.addActionListener(this.btnProducto());
        view.btnVentas.addActionListener(this.btnVentas());
        view.btnTickeds.addActionListener(actionEvent -> {
            try {
                Desktop.getDesktop().open(new File(Config.PATH_TICKEDS));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public ActionListener btnProducto() {
        return actionEvent -> {
            ModProductos modelProductos = new ModProductos();
            ReporteInventario reporte = new ReporteInventario(new ConfigSystem(), modelProductos.consultProductos());
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Guardar reporte de inventario");
            fileChooser.setFileFilter(new FileNameExtensionFilter("PDF file","pdf"));
            int selection = fileChooser.showDialog(view.getParent(), "Save File");
            if (selection == JFileChooser.APPROVE_OPTION) {

                File fileSave = fileChooser.getSelectedFile();
                reporte.setFilePath(fileSave.getAbsolutePath());
                try {
                    reporte.creatFile();
                } catch (IOException e) {
                    e.printStackTrace();
                    Message.error("Error", "Se produjo un Error al guardar el archivo");
                }
            }

        };
    }

    public ActionListener btnVentas()
    {
        return actionEvent -> {
            ModVenta mod = new ModVenta();
            ReporteVentas reporte = new ReporteVentas(new ConfigSystem(), mod.consultarVentas());
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Guardar reporte de inventario");
            fileChooser.setFileFilter(new FileNameExtensionFilter("PDF file","pdf"));
            int selection = fileChooser.showDialog(view.getParent(), "Save File");
            if (selection == JFileChooser.APPROVE_OPTION) {

                File fileSave = fileChooser.getSelectedFile();
                reporte.setFilePath(fileSave.getAbsolutePath());
                try {
                    reporte.creatFile();
                } catch (IOException e) {
                    e.printStackTrace();
                    Message.error("Error", "Se produjo un Error al guardar el archivo");
                }
            }
        };
    }
}
