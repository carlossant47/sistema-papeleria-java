import clases.Config;
import clases.Productos;
import controladores.ConMenu;
import controladores.ConProductos;
import controladores.ConVentas;
import controladores.Login;
import models.ModProductos;
import vistas.FrmLogin;
import vistas.ViewMenu;
import vistas.ViewProducto;
import vistas.ViewVentas;

import javax.swing.*;

public class Main {
    public static void main(String [] agrs)
    {
        Config.createDataFolder();
        try {
            // Set System L&F
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        }
        catch (UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            // handle exception
        }
        new Login(new FrmLogin());

    }
}
