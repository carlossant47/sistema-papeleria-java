package models;

import clases.Empleado;
import clases.Usuario;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ModUsuarios {
    DBConnection db;

    public ModUsuarios()
    {
        this.db = new DBConnection();
    }

    public ArrayList<Usuario> consultar(String busqueda)
    {
        ArrayList<Usuario> list = new ArrayList<>();
        String sql = "SELECT usuarios.id AS idUsuario, USER, empleado.`nombre` AS nombre, empleado.id AS idEmpleado, usuarios.`status` AS estado FROM usuarios INNER JOIN empleado " +
                "ON usuarios.`empleado` = empleado.`id` where user like ? ";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, "%" + busqueda + "%");
            ResultSet resultSet = pred.executeQuery();
            while (resultSet.next()){
                Usuario user = new Usuario();
                user.setIdUsuario(resultSet.getInt(1));
                user.setUser(resultSet.getString(2));
                Empleado emp = new Empleado();
                emp.setNombre(resultSet.getString(3));
                emp.setIdEmpleado(resultSet.getInt(4));
                user.setEmp(emp);
                user.setStatus(resultSet.getInt(5));
                list.add(user);
            }
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return list;
    }

    public DefaultTableModel consultar(String busqueda, int id) {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID Usuario");
        model.addColumn("Usuario");
        model.addColumn("Contrasena");
        model.addColumn("Empleado");
        model.addColumn("Estado");

        ArrayList<Usuario> list = this.consultar(busqueda);
        for (Usuario user : list) {
            String status = ((user.getStatus() == 1) ? "Habilitado": "Baja");
            model.addRow(new Object[]{user.getIdUsuario(), user.getUser(), "*****", user.getEmp().getNombre(), status});
        }
        return model;
    }

    public Usuario buscarId(int id)
    {
        Usuario userFinal = new Usuario();
        String sql = "SELECT usuarios.id AS idUsuario, USER, empleado.`nombre` AS nombre, empleado.id AS idEmpleado, usuarios.`status` AS estado FROM usuarios INNER JOIN empleado\n" +
                "ON usuarios.`empleado` = empleado.`id` where usuarios.id = ? ";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, id);
            ResultSet resultSet = pred.executeQuery();
            while (resultSet.next()){
                Usuario user = new Usuario();
                user.setIdUsuario(resultSet.getInt(1));
                user.setUser(resultSet.getString(2));
                Empleado emp = new Empleado();
                emp.setNombre(resultSet.getString(3));
                emp.setIdEmpleado(resultSet.getInt(4));
                user.setEmp(emp);
                user.setStatus(resultSet.getInt(5));
                userFinal = user;
                break;
            }
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return userFinal;
    }



    public boolean insertar(Usuario user)
    {
        boolean exito = false;
        String sql = "insert into usuarios set user = ?, pass = md5(?), empleado = ?, tipo_usuario = 1";
        try{
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, user.getUser());
            pred.setString(2, user.getPass());
            pred.setInt(3, user.getEmp().getIdEmpleado());
            pred.executeUpdate();
            exito = true;
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }

        return exito;
    }



    public boolean actualizar(Usuario user) {
        boolean exito = false;
        String pass = "";
        if(!user.getPass().matches(""))
        {
            pass = "pass = md5(?)";
        }
        String sql = "update usuarios set user = ?, empleado = ?, tipo_usuario = 1, " + pass +" where id = ?";
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, user.getUser());
            pred.setInt(2, user.getEmp().getIdEmpleado());


            if(!user.getPass().matches(""))
            {
                pred.setString(3, user.getPass());
                pred.setInt(4, user.getIdUsuario());
            }
            else
            {
                pred.setInt(3, user.getIdUsuario());
            }
            pred.executeUpdate();
            exito = true;
            db.closeConection(con);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return exito;
    }

    public boolean cambiarStatus(int id, int status)
    {
        boolean exito = false;
        String sql = "update usuarios set status = ? where id = ?";
        int finalstatus = 0;
        if(status == 0)
        {
            finalstatus = 1;
        }
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, finalstatus);
            pred.setInt(2, id);
            pred.executeUpdate();
            exito = true;
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return exito;

    }
}
