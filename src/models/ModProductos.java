package models;

import clases.*;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ModProductos {

    DBConnection db;

    public ModProductos()
    {
        db = new DBConnection();
    }

    public ArrayList<Productos> consultProductos() {
        ArrayList<Productos> list = new ArrayList<>();
        String sql = "SELECT idproducto, descripcion, precio_venta, idlinea, productos.STATUS, proveedores.`id` AS idprovedor, proveedores.`empresa` FROM productos " +
                "INNER JOIN proveedores ON productos.`proveedor` = proveedores.`id`;";
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            ResultSet result = pred.executeQuery();
            while (result.next()) {
                Productos prod = new Productos();
                prod.setId(result.getInt(1));
                prod.setNombreProducto(result.getString(2));
                prod.setPrecio(result.getFloat(3));
                prod.setTipoProducto(result.getInt(4));
                prod.setStatus(result.getInt(5));
                Proveedores prov = new Proveedores();
                prov.setId(result.getInt(6));
                prov.setEmpresa(result.getString(7));
                prod.setProvedor(prov);
                prov = null;

                list.add(prod);
            }
            db.closeConection(con);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public DefaultComboBoxModel consultarProductos()
    {
        ArrayList<Productos> list = this.consultProductos();
        DefaultComboBoxModel<Productos> model = new DefaultComboBoxModel<>();
        for(Productos prod: list)
        {
            model.addElement(prod);
        }
        return model;
    }

    public DefaultTableModel consultProductos(String busqueda)
    {
        ArrayList<Productos> list = consultProductos();

        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID Producto");
        model.addColumn("Nombre Producto");
        model.addColumn("Precio");
        model.addColumn("Tipo Producto");
        model.addColumn("Cambio de Estado");
        for (Productos prod: list) {
            String status = ((prod.getStatus() == 1) ? "Habilitado": "Baja");
            model.addRow(new Object[]{
                    prod.getId(),
                    prod.getNombreProducto(),
                    String.valueOf("$" + prod.getPrecio()),
                    prod.getTipoProducto(),
                    status
            });
        }
        return model;

    }

    public boolean updateProducto(Productos prod)
    {
        boolean exito = false;
        final String sql = "update productos set descripcion = ?, idlinea = ?, precio_venta = ?, proveedor = ? where idproducto = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, prod.getNombreProducto());
            pred.setInt(2, prod.getTipoProducto());
            pred.setFloat(3, prod.getPrecio());
            pred.setInt(4, prod.getProvedor().getId());
            pred.setInt(5, prod.getId());
            pred.executeUpdate();
            db.closeConection(con);
            exito = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exito;
    }

    public Productos buscarProductoID(int id)
    {
        Productos prodres = null;
        String sql = "SELECT idproducto, descripcion, precio_venta, idlinea, STATUS FROM productos where idproducto = ?;";
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, id);
            ResultSet result = pred.executeQuery();
            while (result.next()) {
                Productos prod = new Productos();
                prod.setId(result.getInt(1));
                prod.setNombreProducto(result.getString(2));
                prod.setPrecio(result.getFloat(3));
                prod.setTipoProducto(result.getInt(4));
                prod.setStatus(result.getInt(5));
                prodres = prod;
            }
            db.closeConection(con);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return prodres;
    }

    public boolean insertProducto(Productos prod)
    {
        boolean exito = false;
        final String sql = "insert into productos set descripcion = ?, idlinea = ?, precio_venta = ?, proveedor = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, prod.getNombreProducto());
            pred.setInt(2, prod.getTipoProducto());
            pred.setFloat(3, prod.getPrecio());
            pred.setInt(4, prod.getProvedor().getId());
            pred.executeUpdate();
            db.closeConection(con);
            exito = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exito;
    }

    public boolean changeStatus(int IdProducto, int status)
    {
        boolean exito = false;
        String sql = "update productos set status = ? where idproducto = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, status);
            pred.setInt(2, IdProducto);
            pred.executeUpdate();
            db.closeConection(con);
            exito = true;
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return exito;
    }

    public DefaultComboBoxModel consultProveedores()
    {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        ArrayList<Proveedores> list = new ModProveedores().consultProveedores("");
        for(Proveedores prov: list)
        {
            model.addElement(prov);
        }

        return model;
    }

    public boolean cambiarStatus(int id, int status)
    {
        boolean exito = false;
        final String sql = "update productos set status = ? where idproducto = ?";
        int finalstatus = 0;
        if(status == 0)
        {
            finalstatus = 1;
        }
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);

            pred.setInt(1, finalstatus);
            pred.setInt(2,id);
            pred.executeUpdate();
            db.closeConection(con);
            exito = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exito;
    }

    public DefaultComboBoxModel consultTipoProducto()
    {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        String sql = "SELECT id, tipo FROM tipo_producto";
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            ResultSet result = pred.executeQuery();
            while (result.next()) {
                TipoProducto tipo = new TipoProducto(result.getInt(1), result.getString(2));
                model.addElement(tipo);

            }
            db.closeConection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return model;
    }



}
