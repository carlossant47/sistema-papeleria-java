package models;

import clases.Empleado;
import clases.Productos;
import clases.Venta;
import clases.Venta_Relacion;

import java.sql.*;
import java.util.ArrayList;

public class ModVenta {

    private DBConnection db;

    public ModVenta() {
        db = new DBConnection();
    }

    public boolean insertVentas(Venta ven) {
        boolean exito = false;
        String sql = "insert into detalle_venta set idventa = ?, cantidad = ?, producto = ?, subtotal = ?";
        try {
            int idVenta = insertVenta(ven);
            if (idVenta > 0) {
                Connection con = db.openConection();
                for (Venta_Relacion index : ven.getListaVenta()) {

                    PreparedStatement pred = con.prepareStatement(sql);
                    pred.setInt(1, idVenta);
                    pred.setInt(2, index.getCantidad());
                    pred.setInt(3, index.getProducto().getId());
                    pred.setFloat(4, index.getSubtotal());
                    //System.out.println(pred);
                    pred.executeUpdate();
                }
                exito = true;
                db.closeConection(con);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return exito;
    }

    private int insertVenta(Venta ven) {
        boolean exito = false;
        int num = 0;
        int idOrden = 0;
        String sql = "INSERT into ventas set empleado = 1, total = " + ven.getTotal();
        try {
            Connection con = db.openConection();
            Statement pred = con.createStatement();
            num = pred.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet resultSet = pred.getGeneratedKeys();
            while (resultSet.next()) {
                idOrden = resultSet.getInt(1);
                System.out.println("ID " + idOrden);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return idOrden;
    }

    public ArrayList<Venta> consultarVentas() {
        ArrayList<Venta> list = new ArrayList<>();
        String sql = "SELECT ventas.`id` AS idVenta, fecha, empleado.`nombre`, total FROM ventas " +
                "INNER JOIN empleado ON ventas.empleado = empleado.id";
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            ResultSet resultSet = pred.executeQuery();
            while (resultSet.next()) {
                Venta ven = new Venta();
                ven.setIdVenta(resultSet.getInt(1));
                ven.setFecha(resultSet.getString(2));
                ven.setEmp(new Empleado(resultSet.getString(3)));
                ven.setTotal(resultSet.getFloat(4));
                ven.setListaVenta(this.consultDetalleVenta(ven.getIdVenta()));
                list.add(ven);
            }
            db.closeConection(con);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return list;

    }

    public ArrayList<Venta_Relacion> consultDetalleVenta(int id) {
        ArrayList<Venta_Relacion> list = new ArrayList<>();
        String sql = "SELECT detalle_venta.id AS idv, productos.`descripcion`, productos.precio_venta, cantidad FROM detalle_venta " +
                "INNER JOIN productos ON detalle_venta.`producto` = productos.`idproducto` WHERE idventa = ?";
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, id);
            ResultSet result = pred.executeQuery();
            while (result.next()) {
                Venta_Relacion ven = new Venta_Relacion();
                ven.setVentar(result.getInt(1));
                ven.getProducto().setNombreProducto(result.getString(2));
                ven.getProducto().setPrecio(result.getFloat(3));
                ven.setCantidad(result.getInt(4));
                list.add(ven);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }
}
