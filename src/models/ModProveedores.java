package models;

import clases.Proveedores;

import javax.swing.table.DefaultTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ModProveedores {

    DBConnection db;

    public ModProveedores()
    {
        db = new DBConnection();
    }


    public ArrayList<Proveedores> consultProveedores(String busqueda) {
        String sql = "SELECT id, empresa, direccion, telefono, STATUS AS estado FROM proveedores WHERE empresa LIKE ?";
        ArrayList<Proveedores> list = new ArrayList<>();
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, "%" + busqueda + "%");
            ResultSet result = pred.executeQuery();
            while (result.next()) {
                Proveedores prov = new Proveedores();
                prov.setId(result.getInt(1));
                prov.setEmpresa(result.getString(2));
                prov.setDireccion(result.getString(3));
                prov.setTelefono(result.getString(4));
                prov.setStatus(result.getInt(5));
                list.add(prov);
            }
            db.closeConection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public boolean insertar(Proveedores prov)
    {
        boolean exito = false;
        String sql = "insert into proveedores set empresa = ?,direccion = ?, telefono = ? ";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, prov.getEmpresa());
            pred.setString(2, prov.getDireccion());
            pred.setString(3, prov.getTelefono());

            pred.executeUpdate();
            exito = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exito;
    }

    public boolean actualizar(Proveedores prov)
    {
        boolean exito = false;
        String sql = "update proveedores set empresa = ?,direccion = ?, telefono = ? where id = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, prov.getEmpresa());
            pred.setString(2, prov.getDireccion());
            pred.setString(3, prov.getTelefono());
            pred.setInt(4, prov.getId());
            pred.executeUpdate();
            exito = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exito;
    }

    public Proveedores buscarId(int id)
    {
        String sql = "SELECT id, empresa, direccion, telefono, STATUS AS estado FROM proveedores WHERE id = ?";
        Proveedores list = new Proveedores();
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, id);
            ResultSet result = pred.executeQuery();
            while (result.next()) {
                Proveedores prov = new Proveedores();
                prov.setId(result.getInt(1));
                prov.setEmpresa(result.getString(2));
                prov.setDireccion(result.getString(3));
                prov.setTelefono(result.getString(4));
                prov.setStatus(result.getInt(5));
                list = prov;
            }
            db.closeConection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public DefaultTableModel consultar(String busqueda)
    {
        ArrayList<Proveedores> list = consultProveedores("");
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID Proveedore");
        model.addColumn("Empresa");
        model.addColumn("Direccion");
        model.addColumn("Telefono");
        model.addColumn("Estado");
        for(Proveedores prov: list)
        {
            String status = ((prov.getStatus() == 1) ? "Habilitado": "Baja");
            model.addRow(new Object[]{prov.getId(), prov.getEmpresa(), prov.getDireccion(), prov.getTelefono(), status});
        }

        return model;

    }

    public boolean cambiarStatus(int id, int status)
    {
        boolean exito = false;
        String sql = "update proveedores set status = ? where id = ?";
        int finalstatus = 0;
        if(status == 0)
        {
            finalstatus = 1;
        }
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, finalstatus);
            pred.setInt(2, id);
            pred.executeUpdate();
            exito = true;
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return exito;

    }





}
