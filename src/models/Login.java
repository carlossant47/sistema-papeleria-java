package models;

import clases.Empleado;
import clases.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Login {
    private String usuario;
    private String password;
    private DBConnection db;

    public Login(String usuario, String password)
    {
        this.db = new DBConnection();
        this.password = password;
        this.usuario = usuario;
    }

    public Empleado loginAction() {
        Empleado user = null;
        String sql = "select USER AS usuario, empleado.`id` AS idEmpleado, empleado.`nombre` AS nombre, usuarios.status as estadouser " +
                "from usuarios INNER JOIN empleado ON usuarios.`empleado` = empleado.`id` " +
                "where md5(user) = md5(?) and pass = md5(?)";
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, usuario);
            pred.setString(2, password);
            ResultSet result = pred.executeQuery();
            while (result.next())
            {
                Empleado obj = new Empleado();
                obj.setNombre(result.getString("nombre"));
                obj.setIdEmpleado(result.getInt("idEmpleado"));
                obj.getUsuario().setUser(result.getString("usuario"));
                obj.getUsuario().setStatus(result.getInt("estadouser"));
                user = obj;
                break;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return user;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
