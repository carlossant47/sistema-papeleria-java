package models;

import clases.Empleado;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ModEmpleados {
    DBConnection db;

    public ModEmpleados()
    {
        db = new DBConnection();
    }

    public boolean insertEmpleado(Empleado emp)
    {
        boolean exito = false;
        String sql = "insert into empleado set nombre = ?, apellidos = ?, telefono = ?, correo = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, emp.getNombre());
            pred.setString(2, emp.getApellidos());
            pred.setString(3, emp.getTelefono());
            pred.setString(4, emp.getCorreo());
            pred.executeUpdate();
            exito = true;
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }

        return exito;
    }

    public boolean cambiarStatus(int id, int status)
    {
        boolean exito = false;
        String sql = "update empleado set status = ? where id = ?";
        int finalstatus = 0;
        if(status == 0)
        {
            finalstatus = 1;
        }
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, finalstatus);
            pred.setInt(2, id);
            pred.executeUpdate();
            exito = true;
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return exito;

    }
    public boolean updateEmpleado(Empleado emp)
    {
        boolean exito = false;
        String sql = "update empleado set nombre = ?, apellidos = ?, telefono = ?, correo = ? where id = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, emp.getNombre());
            pred.setString(2, emp.getApellidos());
            pred.setString(3, emp.getTelefono());
            pred.setString(4, emp.getCorreo());
            pred.setInt(5, emp.getIdEmpleado());
            pred.executeUpdate();
            exito = true;
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return exito;

    }



    private ArrayList<Empleado> consultEmpleados(String busqueda)
    {
        ArrayList<Empleado> list = new ArrayList<>();
        String sql = "SELECT id, nombre, apellidos, telefono, correo, status FROM empleado WHERE nombre LIKE ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, "%" + busqueda + "%");
            ResultSet result = pred.executeQuery();
            while (result.next()){
                Empleado emp = new Empleado();
                emp.setIdEmpleado(result.getInt(1));
                emp.setNombre(result.getString(2));
                emp.setApellidos(result.getString(3));
                emp.setTelefono(result.getString(4));
                emp.setCorreo(result.getString(5));
                emp.setStatus(result.getInt(6));
                list.add(emp);
            }
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return list;
    }

    public Empleado buscarId(int id)
    {
        Empleado empleado = new Empleado();
        String sql = "SELECT id, nombre, apellidos, telefono, correo, status FROM empleado WHERE id = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, id);
            ResultSet result = pred.executeQuery();
            while (result.next()){
                Empleado emp = new Empleado();
                emp.setIdEmpleado(result.getInt(1));
                emp.setNombre(result.getString(2));
                emp.setApellidos(result.getString(3));
                emp.setTelefono(result.getString(4));
                emp.setCorreo(result.getString(5));
                emp.setStatus(result.getInt(6));
                empleado = emp;
                break;
            }
            db.closeConection(con);
        }catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        return empleado;
    }

    public DefaultTableModel consultar(String busquedad)
    {
        ArrayList<Empleado> list = this.consultEmpleados(busquedad) ;
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID Empleado");
        model.addColumn("Nombre");
        model.addColumn("Apellidos");
        model.addColumn("Telefono");
        model.addColumn("Correo");
        model.addColumn("Estado");

        for (Empleado emp: list) {
            String status = ((emp.getStatus() == 1) ? "Habilitado": "Baja");
            model.addRow(new Object[]{

                    emp.getIdEmpleado(),
                    emp.getNombre(),
                    emp.getApellidos(),
                    emp.getTelefono(),
                    emp.getCorreo(),
                    status

            });
        }
        return model;

    }


    public DefaultComboBoxModel consultEmpleados()
    {
        ArrayList<Empleado> list = consultEmpleados("");
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        for (Empleado emp: list){
            model.addElement(emp);
        }

        return model;
    }




}
